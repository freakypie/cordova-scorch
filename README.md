Getting started with cordova

1. install cordova


```
#!bash

    # LINUX
    sudo apt-get install npm # linux: will install nodejs

    # MAC OSX
    # install brew fires on mac osx
    # http://brew.sh/
    # ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
    brew install nodejs # mac osx: or npm not sure which

    # NPM is Node Package Manager. It installs javascript packages
    npm install cordova
    
```



2. create project

    cordova create scorch
    
3. code project

    # hack hack hack
    # in www/
    
4. test in browser, just drag the index.html file to your browser

5. test in device

    # android requires android SDK (put on the path
    # ios requires a lot of crap
    cordova platform add (android|ios)
    cordova run
    
5. test on emulator

    cordova emulate
    
6. release for app store

    cordova build --release
    
    
don't commit platforms/ or plugins/, but you DO need the folder
so add a .gitkeep file and add that for the folders
    
Fab tools: https://bitbucket.org/freakypie/fab-cordova
includes goodies for packaging and distributing apps # multiple apps


Important files
config.xml: tells cordova how to deploy on different platforms
index.html: initial file

Plugins:

cordova plugin add <url>